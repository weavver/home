(function (window) {
     window.__env = window.__env || {};

     // API url
     window.__env.api_url = 'https://localhost/api';
     // window.__env.api_url = "https://api.home.weavver.com";

     // Whether or not to enable debug mode
     // Setting this to false will disable console output
     window.__env.enableDebug = true;
}(this));